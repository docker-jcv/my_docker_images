FROM gcc:10.2.0 AS buildstage

LABEL maintainer="Jorge Camarero Vera"
LABEL maintainer_mail="jcamarerov@gmail.com"

# Set the working directory
WORKDIR /build

# CMake
RUN set -ex; \
  wget https://github.com/Kitware/CMake/releases/download/v3.18.3/cmake-3.18.3-Linux-x86_64.tar.gz; \
  tar -zxvf cmake-3.18.3-Linux-x86_64.tar.gz; \
  mv cmake-3.18.3-Linux-x86_64 cmake; \
  cp -r cmake/* /usr;

# Google Test and Mock
RUN set -ex; \
  git clone https://github.com/google/googletest.git; \
  cd googletest; \
  git checkout release-1.10.0; \
  mkdir build && cd build; \
  cmake .. -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/build/local/; \
  make -j$((`nproc`-2)); \
  make install;

### Development Stage
FROM gcc:10.2.0

LABEL maintainer="Jorge Camarero Vera"
LABEL maintainer_mail="jcamarerov@gmail.com"

COPY --from=buildstage /build/cmake/ /usr/
COPY --from=buildstage /build/local/ /usr/local/

RUN apt-get update; \
  apt-get install -y gdb gdbserver; \
  rm -rf /var/lib/apt/lists/*

RUN set -ex; \
  useradd -ms /bin/bash develop;

# for gdbserver
EXPOSE 2000

USER develop
VOLUME "/home/develop/project"
WORKDIR /home/develop/project
