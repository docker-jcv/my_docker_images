# My Docker Images

Project with my custom docker images

### Images

* __cpp-dev__ : C++ development with gcc 10.2.0 and CMake

### Tutorial

* Registry creation
```Bash
docker run -d \
  -p 5000:5000 \
  --restart=always \
  --name registry \
  registry:2
```
* Volume creation
```Bash
docker volume create gitlab-runner-config
```
* Runner creation
```Bash
docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v gitlab-runner-config:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest
```
* Registration to use Docker in Docker [Use Docker socket binding](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding)
```Bash
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register -n \
  --url https://gitlab.com/ \
  --registration-token oFx5SpuqgWDBNKr5cfdh \
  --executor docker \
  --tag-list jcv_docker \
  --description "My Docker Runner" \
  --docker-image "docker:19.03.13" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock
  ```
